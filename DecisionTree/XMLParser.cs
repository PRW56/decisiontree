﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace DecisionTree
{
    public class XMLParser
    {
        enum ReadingState
        {
            Name,
            AtributeName,
            AtributeValue,
            Ending,
            Children
        }
        
        static Stack<Node> nodeStack;

        /// <summary>
        /// Parses tree of nodes from file,
        /// all nodes in file are expected to be children of 1 root node
        /// </summary>
        /// <param name="filePath"></param>
        /// <returns></returns>
        public static Node ParseFile(string filePath)
        {
            StreamReader reader = new StreamReader(filePath);

            nodeStack = new Stack<Node>();
            int inputInt;
            char input;
            string currentWord = "";
            Node firstNode = null;
            while ((inputInt = reader.Read()) != -1)
            {
                input = (char)inputInt;

                switch(input)
                {
                    case '<':
                        if(firstNode == null)
                        {
                            firstNode = ParseNode(reader);
                            if (firstNode.Name != "root")
                                Console.WriteLine("Warning, root node <" + firstNode.Name + "> does not have the name root.");
                            reader.Close();
                            return firstNode;
                        }
                        break;
                }
            }
            return firstNode;
        }

        /// <summary>
        /// Read in a Node and its children, parse their contents into node objects
        /// </summary>
        /// <param name="reader"></param>
        /// <returns></returns>
        private static Node ParseNode(StreamReader reader)
        {
            Node currentNode = null;
            int inputInt;
            char input;
            string currentWord = "";
            string currentAttributeName = "";
            bool isEndNode = false;
            ReadingState readingState = ReadingState.Name;
            while ((inputInt = reader.Read()) != -1)
            {
                input = (char)inputInt;

                switch (input)
                {
                    case '<':
                        //This should only be read in if looking for children/end node
                        if(readingState == ReadingState.Children)
                        {
                            Node nextNode = ParseNode(reader);
                            if (nextNode.isEndNode)
                            {
                                if (nextNode.Name != currentNode.Name)
                                    Console.WriteLine("Warning End node for <" + currentNode.Name + "> did not have the same name. End node: <" + nextNode.Name + ">");
                                Console.WriteLine("Node: " + currentNode.Name + " was read in.");
                                return currentNode;
                            }
                            else
                                currentNode.children.Add(nextNode);
                        }
                        break;
                    case ' ':
                        if (readingState == ReadingState.Name)
                        {
                            currentNode = new Node(currentWord);
                            currentWord = "";
                            readingState = ReadingState.AtributeName;
                        }
                        else if(currentWord != "" && (readingState == ReadingState.AtributeName || readingState == ReadingState.AtributeValue))
                        {
                            currentWord += input;
                        }
                        break;
                    case '>':
                        
                        if (isEndNode)
                        {
                            currentNode = new Node(currentWord);
                            currentNode.isEndNode = true;
                            Console.WriteLine("Node: " + currentNode.Name + " was read in.");
                            return currentNode;
                        }
                        //the node has children, or at least a seperate end node
                        else if (readingState == ReadingState.Name)
                        {
                            currentNode = new Node(currentWord);
                            currentWord = "";
                            readingState = ReadingState.Children;
                        }
                        else if (readingState == ReadingState.AtributeName)
                        {
                            currentWord = "";
                            readingState = ReadingState.Children;
                        }
                        //note that this state will be reached every time a child node is read in

                        break;
                    case '/':
                        //if / is reached while still reading the name, 
                        //the node should be complete, and have no children
                        if (readingState == ReadingState.AtributeName)
                        {
                            Console.WriteLine("Node: " + currentNode.Name + " was read in, had no children.");
                            return currentNode;
                        }
                        //current node is an end node
                        else if(readingState == ReadingState.Name && currentNode == null)
                        {
                            isEndNode = true;
                        }
                        break;
                    case '=':
                        if (readingState == ReadingState.AtributeName)
                        {
                            currentAttributeName = currentWord;
                            currentWord = "";
                            readingState = ReadingState.AtributeValue;
                        }
                        break;
                    case '\"':
                        if(readingState == ReadingState.AtributeValue)
                        {
                            //for every attribute value the first " should be ignored
                            if (currentWord == "")
                            {
                                currentWord += input; //had to do this to account for empty string values
                                break;
                            }

                            currentNode.attributes.Add(currentAttributeName, currentWord.Substring(1));
                            currentWord = "";
                            readingState = ReadingState.AtributeName;
                        }
                        break;
                    default:
                        currentWord += input;
                        break;
                }
            }

            Console.WriteLine("Node: " + currentNode.Name + " was read in. Something went wrong.");
            return currentNode; //if this was reached something went wrong
        }
    }
}
