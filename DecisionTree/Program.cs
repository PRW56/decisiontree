﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace DecisionTree
{
    public class Program
    {
        public static void Main(string[] args)
        {
            string input = "";
            while (true)
            {
                Console.Write("Enter absolute file path of XML file (or quit to close): ");
                input = Console.ReadLine();

                if (input.ToLower() == "quit")
                    break;

                if (File.Exists(input))
                    break;
                else
                    Console.WriteLine("Invalid file path specified");
            }

            Node rootNode = XMLParser.ParseFile(input);

            rootNode.PrintTree();

            input = "";
            while(true)
            {
                Console.Write("Enter behavior (or quit to close): ");
                input = Console.ReadLine();

                if (input.ToLower() == "quit")
                    break;

                int dsteps = 0;
                string dresponse = "";
                if (rootNode.DepthFirstLookup(input, ref dresponse, ref dsteps))
                    Console.WriteLine("DFS response found in " + dsteps + " steps.");
                else
                    Console.WriteLine("DFS did not find a node with specified behavior.");
                int bsteps = 0;
                string bresponse = "";
                if (rootNode.BreadthFirstLookup(input, ref bresponse, ref bsteps))
                    Console.WriteLine("BFS response found in " + bsteps + " steps.");
                else
                    Console.WriteLine("BFS did not find a node with specified behavior.");

                Console.WriteLine("response: " + dresponse);
            }
        }
    }
}
