﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DecisionTree
{
    public class Node
    {
        string name;
        public Dictionary<string, string> attributes;
        public List<Node> children;
        public bool isEndNode = false;

        private const string BEHAVIOR_ATTRIBUTE_NAME = "behavior";
        private const string RESPONSE_ATTRIBUTE_NAME = "response";

        static Random rand = new Random();

        public string Name
        {
            get { return name; }
        }

        public Node(string name)
        {
            this.name = name;
            attributes = new Dictionary<string, string>();
            children = new List<Node>();
        }

        public string this[string attributeName]
        {
            get { return attributes[attributeName]; }
            set { attributes[attributeName] = value; }
        }

        public bool DepthFirstLookup(string behavior, ref string response, ref int stepCount)
        {
            response = "";
            stepCount++;
            //if the current node matches the specified behavior return its response
            if (GetResponse(behavior, ref response))
                return true;

            //otherwise search its children for a response
            foreach(Node child in children)
            {
                //ignore children that do not have a behavior attribute, note that I strongly disagree with the format of the sample xml
                //propose tags have unique names to specify if they hold a behavior or a response, rather than every tag (with the exception of root) be called node
                if (child.attributes.ContainsKey(BEHAVIOR_ATTRIBUTE_NAME) && child.attributes[BEHAVIOR_ATTRIBUTE_NAME] == "")
                    continue;
                if(child.DepthFirstLookup(behavior, ref response, ref stepCount))
                    return true;
            }

            //specified behavior was not found
            return false;
        }

        public bool BreadthFirstLookup(string behavior, ref string response, ref int stepCount)
        {
            Queue<Node> searchQueue = new Queue<Node>(); //had to use this so they end up in the right order
            searchQueue.Enqueue(this);

            stepCount = 0;
            while(searchQueue.Count != 0)
            {
                stepCount++;
                Node currentNode = searchQueue.Dequeue();
                //test the current node
                if (currentNode.GetResponse(behavior, ref response))
                    return true;
                //queue all its children to be searched
                foreach (Node child in currentNode.children)
                {
                    if (child.attributes.ContainsKey(BEHAVIOR_ATTRIBUTE_NAME) && child.attributes[BEHAVIOR_ATTRIBUTE_NAME] == "")
                        continue;
                    searchQueue.Enqueue(child);
                }
            }

            return false;
        }

        private bool GetResponse(string behavior, ref string response)
        {
            if (attributes.ContainsKey(BEHAVIOR_ATTRIBUTE_NAME) && attributes[BEHAVIOR_ATTRIBUTE_NAME].ToLower() == behavior.ToLower())
            {
                if (children.Count == 0)
                {
                    Console.WriteLine("Warning, specified behavior does not have a response.");
                    response = "";
                    return true;
                }

                //!! response is not located on the node, it is located in its children !!
                //response = attributes[RESPONSE_ATTRIBUTE_NAME];

                //compile possible responses
                List<string> possibleResponses = new List<string>();
                GetResponses(ref possibleResponses);

                int responseIndex;
                if (possibleResponses.Count == 1)
                    responseIndex = 0;
                else
                    responseIndex = rand.Next(possibleResponses.Count);

                response = possibleResponses[responseIndex];
                return true;
            }
            return false;
        }

        private void GetResponses(ref List<string> responses)
        {
            if (attributes.ContainsKey(RESPONSE_ATTRIBUTE_NAME) && attributes[RESPONSE_ATTRIBUTE_NAME] != "")
                responses.Add(attributes[RESPONSE_ATTRIBUTE_NAME]);

            foreach (Node child in children)
                child.GetResponses(ref responses);
        }

        public void PrintTree()
        {
            Console.WriteLine("\nTree:");
            Console.WriteLine(this.GetTabbedString(0));

        }

        public string GetTabbedString(int depth)
        {
            string padding = "";
            for (int x = 0; x < depth; x++)
                padding += "\t";

            string outString = "\n"+padding+"<" + name;

            foreach(KeyValuePair<string, string> entry in attributes)
            {
                outString += " " + entry.Key + "=\"" + entry.Value + "\"";
            }
            if(children.Count > 0)
            {
                outString += ">";
                foreach(Node child in children)
                {
                    outString += child.GetTabbedString(depth +1 );
                }
                outString += "\n"+padding+"</" + name + ">";
            }
            else
            {
                outString += "/>";
            }
            return outString;
        }
    }
}
